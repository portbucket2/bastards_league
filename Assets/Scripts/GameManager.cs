using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using GameAnalyticsSDK;
using LionStudios.Suite.Analytics;

public class GameManager : MonoBehaviour
{
    public TMP_Text level_num;
    public GameObject finish_him_text;
    public GameObject victory_text;
    public GameObject fail_ui;

    public float map_left_limit;
    public float map_right_limit;

    public int slow_down_counter;

    public bool level_completed = false;
    public bool level_failed = false;

    private void Start()
    {
        level_num.text = "Level " + (Stats.currentFakeLevel).ToString();
        Application.targetFrameRate = 60;

        PlayerPrefs.SetInt("CurrentFakeLevel", Stats.currentFakeLevel);
        LionAnalytics.LevelStart(Stats.currentFakeLevel, Stats.attemptNum);
    }
    public void Restart()
    {
        Time.timeScale = 1f;

        LionAnalytics.LevelRestart(Stats.currentFakeLevel, Stats.attemptNum);
        
        Stats.attemptNum++;

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void FinishHim()
    {
        finish_him_text.SetActive(true);
    }

    public void LevelEnd()
    {
        finish_him_text.SetActive(false);
        victory_text.SetActive(true);

        level_completed = true;

        LionAnalytics.LevelComplete(Stats.currentFakeLevel, Stats.attemptNum);
    }

    public void Next()
    {
        Stats.attemptNum = 1;

        SceneManager.LoadScene(Stats.CalculateNextLevel(1));
    }

    public void Failed()
    {
        level_failed = true;
        fail_ui.SetActive(true);

        LionAnalytics.LevelFail(Stats.currentFakeLevel, Stats.attemptNum);
    }
}
