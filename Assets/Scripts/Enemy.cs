using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float player_min_dist;
    public Animator animator;
    public float movement_speed;

    public float rand_action_dur_min;
    public float rand_action_dur_max;

    private Avatar avatar;

    public float reset_speed;

    public List<Muscle> muscles;
    public List<PositionFollower> positionFollow;

    public Rigidbody2D spine;

    private Rigidbody2D rb;
    private Vector2 intended_position = Vector2.zero;

    private PlayerController player;

    private GameManager game_manager;

    private bool is_ragdoll = false;
    private float temp_ragdoll_cooldown = 0f;

    private void Start()
    {
        game_manager = FindObjectOfType<GameManager>();
        intended_position = transform.position;
        rb = GetComponent<Rigidbody2D>();
        player = FindObjectOfType<PlayerController>();

        avatar = GetComponent<Avatar>();

        avatar.health_bar.max_health = avatar.max_health;
        avatar.health_bar.Initiate();

        foreach (Muscle muscle in muscles)
        {
            muscle.hinge = muscle.rb.GetComponent<HingeJoint2D>();
        }
        foreach (PositionFollower follower in positionFollow)
        {
            follower.hinge = follower.rb.GetComponent<HingeJoint2D>();
        }

        Invoke("RandomAction", Random.Range(rand_action_dur_min, rand_action_dur_max));
    }


    private void Attack()
    {
        animator.Play("attack_1", -1, 0);
    }

    public void MoveRight()
    {
        intended_position += Vector2.right * 2f;
        animator.Play("walk_backward", -1, 0);
    }

    public void MoveLeft()
    {
        intended_position += Vector2.left * 2f;
        animator.Play("walk_forward", -1, 0);
    }

    private void RandomAction()
    {
        if (avatar.is_almost_dead)
        {
            animator.enabled = false;
            return;
        }

        if (Mathf.Abs(transform.position.x - player.transform.position.x) > 6)
        {
            MoveLeft();
            Invoke("RandomAction", 0.5f);
        }
        else if (Mathf.Abs(transform.position.x - player.transform.position.x) < player_min_dist)
        {
            MoveRight();
            Invoke("RandomAction", 0.5f);
        }
        else
        {
            int action = Random.Range(0, 6);

            if (action == 0)
            {
                if (player.transform.position.x < transform.position.x - 1)
                {
                    MoveRight();
                    //Attack();
                }
                else
                {
                    MoveLeft();
                }
            }
            else if (action == 1)
            {
                if (transform.position.x < game_manager.map_right_limit)
                {
                    MoveRight();
                    //Attack();
                }
                else
                {
                    MoveLeft();
                }
            }
            else if (action > 2)
            {
                Attack();
            }

            Invoke("RandomAction", Random.Range(rand_action_dur_min, rand_action_dur_max));
        }

    }

    private void FixedUpdate()
    {
        if (avatar.is_dead)
        {
            return;
        }

        if (temp_ragdoll_cooldown > 0)
        {
            Vector2 target_position = rb.position;
            target_position.x = spine.position.x;
            rb.position = target_position;
            intended_position = rb.position;

            temp_ragdoll_cooldown -= Time.fixedDeltaTime;
            return;
        }

        foreach (Muscle muscle in muscles)
        {
            muscle.ActivateMuscle();
        }

        foreach (PositionFollower follower in positionFollow)
        {
            follower.FollowPosition();
        }

        rb.MovePosition(Vector2.Lerp(rb.position, intended_position, Time.deltaTime * movement_speed));
    }


    public void TemporaryRagdoll(float duration)
    {
        //is_ragdoll = true;

        //Invoke("TemporaryRagdollOff", duration);

        temp_ragdoll_cooldown = duration;
    }

    private void TemporaryRagdollOff()
    {
        is_ragdoll = false;
    }
}
