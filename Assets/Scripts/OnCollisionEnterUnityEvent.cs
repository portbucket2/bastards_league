using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnCollisionEnterUnityEvent : MonoBehaviour
{
    public string compare_this_tag;
    public UnityEvent do_these;

    public float reset_delay;

    private bool collided_once = false;

    private void OnCollisionEnter(Collision collision)
    {
        if (collided_once)
        {
            return;
        }
        
        collided_once = true;

        do_these?.Invoke();
        Invoke("ResetCollision", reset_delay);
    }

    private void ResetCollision()
    {
        collided_once = false;
    }
}
