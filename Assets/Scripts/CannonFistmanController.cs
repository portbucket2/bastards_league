using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D.IK;
using UnityEngine.Events;

public class CannonFistmanController : MonoBehaviour
{
    public GameObject cannon_mask_front;
    public GameObject cannon_mask_back;

    public float cannon_power;
    public float push_back;
    public float push_up;
    public List<Rigidbody2D> cannon_back = new List<Rigidbody2D>();
    private int cannon_back_current = 0;
    public List<CannonFist> cannon_fist_back = new List<CannonFist>();
    public Transform cannon_back_spawner;
    public List<Rigidbody2D> cannon_front = new List<Rigidbody2D>();
    public List<CannonFist> cannon_fist_front = new List<CannonFist>();
    private int cannon_front_current = 0;
    public Transform cannon_front_spawner;

    public float cannon_windup;
    private float cannon_front_windup = 0;
    private float cannon_back_windup = 0;

    public UnityEvent OnRightStickDown;
    public UnityEvent OnRightStickUp;
    public UnityEvent OnLeftStickDown;
    public UnityEvent OnLeftStickUp;

    public float flight_speed;
    public float flight_accel;
    public float movement_speed;

    public Vector2 gravity;

    public Animator avatar_ik_anim;

    private Rigidbody2D player_rb;

    public float joy_max_distance;
    private Vector2 joystick_right_rest_pos;
    public GameObject joystick_right_base;
    public GameObject joystick_right_handle;
    private Vector2 touch_diff_right;

    private Vector2 joystick_left_rest_pos;
    public GameObject joystick_left_base;
    public GameObject joystick_left_handle;
    private Vector2 touch_diff_left;

    public float joy_diff_multiplier;
    public float joy_diff_multiplier_head;
    public GameObject right_shoulder;
    public GameObject left_shoulder;

    public GameObject forearm_back_target;
    private Vector2 forearm_back_rest;
    public GameObject head_target;
    private Vector2 head_rest;
    public GameObject forearm_front_target;
    private Vector2 forearm_front_rest;

    public float reset_speed;


    public List<Muscle> muscles;

    public List<PositionFollower> positionFollow;

    public Rigidbody2D root;


    private Vector2 intended_position = new Vector2(-2, 0);
    //private Vector2 intended_flight = new Vector2(0, 0);
    //private Vector2 target_flight = new Vector2(0, 0);

    private Vector2 head_movement_offset = Vector2.zero;

    private float double_tap_right_timer = 0f;
    private float double_tap_left_timer = 0f;

    private int right_touch_id = -1;
    private int left_touch_id = -1;

    public Text[] debug_text;

    private Avatar avatar;
    //private Enemy enemy;
    public Rigidbody2D enemy_spine;

    private GameManager game_manager;

    private void Start()
    {
        game_manager = FindObjectOfType<GameManager>();

        //for (int i = 0; i < cannon_back_spawner.transform.childCount; i++)
        //{
        //    cannon_back[i] = cannon_back_spawner.transform.GetChild(i).GetComponent<Rigidbody2D>();
        //    cannon_fist_back[i] = cannon_back_spawner.transform.GetChild(i).GetComponent<CannonFist>();
        //}

        //enemy = FindObjectOfType<Enemy>();
        avatar = GetComponent<Avatar>();

        avatar.health_bar.max_health = avatar.max_health;
        avatar.health_bar.Initiate();

        intended_position = transform.position;

        player_rb = GetComponent<Rigidbody2D>();

        Vector3 offset = Vector3.zero;
        offset.x = transform.position.x;

        forearm_front_rest = forearm_front_target.transform.position - offset;
        forearm_back_rest = forearm_back_target.transform.position - offset;
        head_rest = head_target.transform.position - offset;

        joystick_right_rest_pos = joystick_right_base.transform.position;
        joystick_left_rest_pos = joystick_left_base.transform.position;

        foreach (Muscle muscle in muscles)
        {
            muscle.hinge = muscle.rb.GetComponent<HingeJoint2D>();
        }
        foreach (PositionFollower follower in positionFollow)
        {
            follower.hinge = follower.rb.GetComponent<HingeJoint2D>();
        }
    }

    bool right = false;
    private void Update()
    {
        debug_text[0].text = ((int)(1f / Time.unscaledDeltaTime)).ToString();

        if (double_tap_left_timer > 0)
        {
            double_tap_left_timer -= Time.deltaTime;
        }

        if (double_tap_right_timer > 0)
        {
            double_tap_right_timer -= Time.deltaTime;
        }

        if (cannon_front_windup > 0)
        {
            cannon_front_windup -= Time.deltaTime;
        }

        if (cannon_back_windup > 0)
        {
            cannon_back_windup -= Time.deltaTime;
        }

        cannon_mask_front.transform.localScale = new Vector3((cannon_front_windup / cannon_windup), 1, 1);
        cannon_mask_back.transform.localScale = new Vector3((cannon_back_windup / cannon_windup), 1, 1);

#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            if (Input.mousePosition.x > Screen.width / 2)
            {
                right = true;

                joystick_right_base.SetActive(true);
                joystick_right_base.transform.position = Input.mousePosition;

                cannon_front_windup = cannon_windup;

                OnRightStickDown?.Invoke();

                if (intended_position.x <= enemy_spine.position.x - 2)
                //if (transform.position.x < 10)
                {
                    if (double_tap_right_timer > 0)
                    {
                        //head_movement_offset = Vector2.right * 2;
                        intended_position += Vector2.right * 2;
                        avatar_ik_anim.Play("walk_forward", -1, 0);
                    }
                    else
                    {
                        if (transform.position.y < 0.1)
                        {
                            double_tap_right_timer = 0.5f;
                        }
                    }
                }
            }
            else
            {
                right = false;

                joystick_left_base.SetActive(true);
                joystick_left_base.transform.position = Input.mousePosition;

                cannon_back_windup = cannon_windup;

                OnLeftStickDown?.Invoke();

                if (intended_position.x > game_manager.map_left_limit)
                {
                    if (double_tap_left_timer > 0)
                    {
                        //head_movement_offset = Vector2.left * 2;
                        intended_position += Vector2.left * 2;
                        avatar_ik_anim.Play("walk_backward", -1, 0);
                    }
                    else
                    {
                        if (transform.position.y < 0.1)
                        {
                            double_tap_left_timer = 0.5f;
                        }
                    }
                }
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            Vector2 joy_diff_right = joystick_right_handle.transform.position - joystick_right_base.transform.position;
            Vector2 joy_diff_left = joystick_left_handle.transform.position - joystick_left_base.transform.position;

            if (right)
            {
                if (joy_diff_right.magnitude > 1)
                {
                    CannonShootFront(joy_diff_right);
                }
                OnRightStickUp?.Invoke();
            }
            else
            {
                if (joy_diff_left.magnitude > 1)
                {
                    CannonShootBack(joy_diff_left);
                }
                OnLeftStickUp?.Invoke();
            }

            touch_diff_right = Vector2.zero;
            touch_diff_left = Vector2.zero;
            joystick_right_handle.transform.localPosition = Vector2.zero;
            joystick_left_handle.transform.localPosition = Vector2.zero;

            //joystick_right_base.SetActive(false);
            //joystick_left_base.SetActive(false);

            joystick_right_base.transform.position = joystick_right_rest_pos;
            joystick_left_base.transform.position = joystick_left_rest_pos;

            head_movement_offset = Vector2.zero;
        }

        if (Input.GetMouseButton(0))
        {
            Vector2 joy_diff_right = joystick_right_handle.transform.position - joystick_right_base.transform.position;
            Vector2 joy_diff_left = joystick_left_handle.transform.position - joystick_left_base.transform.position;

            //intended_flight = -(joy_diff_right + joy_diff_left) * joy_diff_multiplier;


            if (right)
            {
                touch_diff_right = Input.mousePosition - joystick_right_base.transform.position;

                if (touch_diff_right.magnitude < joy_max_distance)
                {
                    joystick_right_handle.transform.position = Input.mousePosition;
                }
                else
                {
                    joystick_right_handle.transform.position = (Vector2)joystick_right_base.transform.position + touch_diff_right.normalized * joy_max_distance;
                    joystick_right_base.transform.position = Vector2.Lerp(joystick_right_base.transform.position, Input.mousePosition, Time.deltaTime * 2f);
                }

                forearm_front_target.transform.position = (Vector2)left_shoulder.transform.position + joy_diff_right * joy_diff_multiplier + Vector2.right * 1f;
            }
            else
            {
                touch_diff_left = Input.mousePosition - joystick_left_base.transform.position;

                if (touch_diff_left.magnitude < joy_max_distance)
                {
                    joystick_left_handle.transform.position = Input.mousePosition;
                }
                else
                {
                    joystick_left_handle.transform.position = (Vector2)joystick_left_base.transform.position + touch_diff_left.normalized * joy_max_distance;
                    joystick_left_base.transform.position = Vector2.Lerp(joystick_left_base.transform.position, Input.mousePosition, Time.deltaTime * 2f);
                }

                forearm_back_target.transform.position = (Vector2)right_shoulder.transform.position + joy_diff_left * joy_diff_multiplier + Vector2.right * 1f;
            }

            //head_target.transform.position = (Vector2)transform.position + (joy_diff_right + joy_diff_left) / 1 * joy_diff_multiplier_head + Vector2.up * 3.6f + head_movement_offset;
            //head_target.transform.position = forearm_front_target.transform.position + (forearm_back_target.transform.position - forearm_front_target.transform.position) * 0.5f;
        }
        else
        {
            Vector2 offset = Vector2.zero;
            offset.x = transform.position.x;
            offset.y = transform.position.y;

            forearm_back_target.transform.position = Vector2.Lerp(forearm_back_target.transform.position, forearm_back_rest + offset, Time.deltaTime * reset_speed);
            forearm_front_target.transform.position = Vector2.Lerp(forearm_front_target.transform.position, forearm_front_rest + offset, Time.deltaTime * reset_speed);
            //head_target.transform.position = Vector2.Lerp(head_target.transform.position, head_rest + offset, Time.deltaTime * reset_speed);
        }
#else

        int touch_idx = 0;

        while (touch_idx < Input.touchCount)
        {

            Touch t = Input.GetTouch(touch_idx);

            //if (touch_idx < debug_text.Length)
            //{
            //    debug_text[touch_idx].text = t.position.ToString();
            //}

            if (t.phase == TouchPhase.Began)
            {
                if (t.position.x > Screen.width / 2)
                {
                    joystick_right_base.transform.position = t.position;
                    right_touch_id = t.fingerId;

                    cannon_front_windup = cannon_windup;

                    OnRightStickDown?.Invoke();

                    if (intended_position.x <= enemy_spine.position.x - 2)
                    {
                        if (double_tap_right_timer > 0)
                        {
                            //head_movement_offset = Vector2.right * 2;
                            intended_position += Vector2.right * 2;
                            avatar_ik_anim.Play("walk_forward", -1, 0);
                        }
                        else
                        {
                            if (transform.position.y < 0.1)
                            {
                                double_tap_right_timer = 0.5f;
                            }
                        }
                    }
                }
                else
                {
                    joystick_left_base.transform.position = t.position;
                    left_touch_id = t.fingerId;

                    cannon_back_windup = cannon_windup;

                    OnLeftStickDown?.Invoke();

                    if (intended_position.x > game_manager.map_left_limit)
                    {
                        if (double_tap_left_timer > 0)
                        {
                            //head_movement_offset = Vector2.left * 2;
                            intended_position += Vector2.left * 2;
                            avatar_ik_anim.Play("walk_backward", -1, 0);
                        }
                        else
                        {
                            if (transform.position.y < 0.1)
                            {
                                double_tap_left_timer = 0.5f;
                            }
                        }
                    }
                }
            }
            else if (t.phase == TouchPhase.Ended)
            {
                Vector2 joy_diff_right = joystick_right_handle.transform.position - joystick_right_base.transform.position;
                Vector2 joy_diff_left = joystick_left_handle.transform.position - joystick_left_base.transform.position;

                if (t.fingerId == right_touch_id)
                {
                    CannonShootFront(joy_diff_right);

                    touch_diff_right = Vector2.zero;
                    joystick_right_handle.transform.localPosition = Vector2.zero;
                    joystick_right_base.transform.position = joystick_right_rest_pos;

                    right_touch_id = -1;

                    OnRightStickUp?.Invoke();
                }

                if (t.fingerId == left_touch_id)
                {
                    CannonShootBack(joy_diff_left);

                    touch_diff_left = Vector2.zero;
                    joystick_left_handle.transform.localPosition = Vector2.zero;
                    joystick_left_base.transform.position = joystick_left_rest_pos;

                    left_touch_id = -1;

                    OnLeftStickUp?.Invoke();
                }

                head_movement_offset = Vector2.zero;
            }
            else if (t.phase == TouchPhase.Moved)
            {
                Vector2 joy_diff_right = joystick_right_handle.transform.position - joystick_right_base.transform.position;
                Vector2 joy_diff_left = joystick_left_handle.transform.position - joystick_left_base.transform.position;

                if (t.fingerId == right_touch_id)
                {
                    touch_diff_right = t.position - (Vector2)joystick_right_base.transform.position;

                    if (touch_diff_right.magnitude < joy_max_distance)
                    {
                        joystick_right_handle.transform.position = t.position;
                    }
                    else
                    {
                        joystick_right_handle.transform.position = (Vector2)joystick_right_base.transform.position + touch_diff_right.normalized * joy_max_distance;
                        joystick_right_base.transform.position = Vector2.Lerp(joystick_right_base.transform.position, t.position, Time.deltaTime * 2f);
                    }

                    forearm_front_target.transform.position = (Vector2)left_shoulder.transform.position + joy_diff_right * joy_diff_multiplier + Vector2.right * 1f;
                }


                if(t.fingerId == left_touch_id)
                {
                    touch_diff_left = t.position - (Vector2)joystick_left_base.transform.position;

                    if (touch_diff_left.magnitude < joy_max_distance)
                    {
                        joystick_left_handle.transform.position = t.position;
                    }
                    else
                    {
                        joystick_left_handle.transform.position = (Vector2)joystick_left_base.transform.position + touch_diff_left.normalized * joy_max_distance;
                        joystick_left_base.transform.position = Vector2.Lerp(joystick_left_base.transform.position, t.position, Time.deltaTime * 2f);
                    }

                    forearm_back_target.transform.position = (Vector2)right_shoulder.transform.position + joy_diff_left * joy_diff_multiplier + Vector2.right * 1f;
                }

                //head_target.transform.position = (Vector2)transform.position + (joy_diff_right + joy_diff_left) / 1 * joy_diff_multiplier_head + Vector2.up * 3.6f + head_movement_offset;
                //head_target.transform.position = forearm_front_target.transform.position + (forearm_back_target.transform.position - forearm_front_target.transform.position) * 0.5f;
            
        }

            touch_idx++;
        }


        Vector2 offset = Vector2.zero;
        offset.x = transform.position.x;
        offset.y = transform.position.y;

        if(right_touch_id == -1)
        {
            forearm_front_target.transform.position = Vector2.Lerp(forearm_front_target.transform.position, forearm_front_rest + offset, Time.deltaTime * reset_speed);
        }
        if(left_touch_id == -1)
        {
            forearm_back_target.transform.position = Vector2.Lerp(forearm_back_target.transform.position, forearm_back_rest + offset, Time.deltaTime * reset_speed);
        }

        //if(right_touch_id == -1 && left_touch_id == -1)
        //{
        //    head_target.transform.position = Vector2.Lerp(head_target.transform.position, head_rest + offset, Time.deltaTime * reset_speed);
        //}

#endif


        //avatar_ik.transform.position -= Vector3.left * Time.deltaTime * 2f;

        //Vector2 t = (Vector2)root.transform.position + Vector2.right * Time.deltaTime * 2f;

        //root.MovePosition(t);
        //root.AddForce(Vector2.right * 500f);

        //head_movement_offset = Vector2.Lerp(head_movement_offset, Vector2.zero, Time.deltaTime * 8);


        //target_flight = Vector2.Lerp(target_flight, intended_flight, Time.deltaTime * flight_accel);

        intended_position += gravity * Time.deltaTime;


        if (intended_position.x > enemy_spine.position.x - 2)
        {
            intended_position.x = enemy_spine.position.x - 2;
        }
        if (intended_position.y > 10)
        {
            intended_position.y = 5;
        }
        if (intended_position.x < game_manager.map_left_limit)
        {
            intended_position.x = game_manager.map_left_limit;
        }
        if (intended_position.y < 0)
        {
            intended_position.y = 0;
        }

        //intended_flight = Vector2.zero;
    }

    private void FixedUpdate()
    {
        //player_rb.MovePosition(Vector2.right);
        if (avatar.is_dead)
        {
            return;
        }

        foreach (Muscle muscle in muscles)
        {
            muscle.ActivateMuscle();
        }
        foreach (PositionFollower follower in positionFollow)
        {
            follower.FollowPosition();
        }

        //transform.position = Vector2.Lerp(transform.position, intended_position, Time.deltaTime * movement_speed);
        player_rb.MovePosition(Vector2.Lerp(player_rb.position, intended_position, Time.deltaTime * movement_speed));
    }


    public void CannonShootBack(Vector2 direction)
    {
        if (cannon_back_windup > 0)
        {
            return;
        }

        cannon_back[cannon_back_current].gameObject.SetActive(true);
        cannon_back[cannon_back_current].velocity = Vector2.zero;
        cannon_back[cannon_back_current].angularVelocity = 0;
        cannon_back[cannon_back_current].position = cannon_back_spawner.position;
        cannon_fist_back[cannon_back_current].damaged_once = false;
        cannon_back[cannon_back_current].AddForce(direction.normalized * cannon_power, ForceMode2D.Impulse);

        cannon_back_current++;

        if (cannon_back_current >= cannon_back.Count)
        {
            cannon_back_current = 0;
        }

        intended_position += -direction.normalized * push_back + Vector2.up * push_up;
    }

    public void CannonShootFront(Vector2 direction)
    {
        if (cannon_front_windup > 0)
        {
            return;
        }

        cannon_front[cannon_front_current].gameObject.SetActive(true);
        cannon_front[cannon_front_current].velocity = Vector2.zero;
        cannon_front[cannon_front_current].angularVelocity = 0;
        cannon_front[cannon_front_current].position = cannon_front_spawner.position;
        cannon_fist_front[cannon_front_current].damaged_once = false;
        cannon_front[cannon_front_current].AddForce(direction.normalized * cannon_power, ForceMode2D.Impulse);

        cannon_front_current++;

        if (cannon_front_current >= cannon_front.Count)
        {
            cannon_front_current = 0;
        }

        intended_position += -direction.normalized * push_back + Vector2.up * push_up;
    }
}
