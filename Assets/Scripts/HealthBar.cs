using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Gradient health_color;

    public Image fill;
    public float max_health;
    private Slider slider;

    private void Awake()
    {
        slider = GetComponent<Slider>();
        fill.color = health_color.Evaluate(slider.value / slider.maxValue);
    }

    public void Initiate()
    {
        slider.maxValue = max_health;
        slider.value = max_health;
    }

    public void UpdateHealth(float increment)
    {
        slider.value += increment;
        fill.color = health_color.Evaluate(slider.value / slider.maxValue);
    }

    public float GetHealthPercentage()
    {
        return slider.value / slider.maxValue;
    }
}
