using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageReceiver : MonoBehaviour
{
    public Avatar parent_avatar;

    public void ReceiveDamage(float damage)
    {
        if (parent_avatar.is_almost_dead)
        {
            parent_avatar.Finisher();
        }
        else
        {
            parent_avatar.ReceiveDamage(damage);
        }

    }
}
